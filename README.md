cd commerce_culqi/

# Run

```bash
$ composer install
```

### Test Data

<table>
  <tr>
    <td>Card Number</td>
    <td>4111 1111 1111 1111</td>
  </tr>
  <tr>
    <td>Date</td>
    <td>9/20</td>
  </tr>
  <tr>
    <td>CVV</td>
    <td>123</td>
  </tr>
</table>

# Licencia

Copyright 2017 Culqi

Licensed under the MIT License


# Configuration:
# -------------
Go to "Store" -> "Configuration" -> "Payment methods" ->  to find
all the configuration options.

## [View demo](http://culqi.drupal.pe/)